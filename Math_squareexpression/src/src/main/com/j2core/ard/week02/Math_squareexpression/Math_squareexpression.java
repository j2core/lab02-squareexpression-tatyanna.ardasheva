package src.main.com.j2core.ard.week02.Math_squareexpression;

import java.util.InputMismatchException;
import java.util.Scanner;
// It's my first project. Please, point out errors.


public class Math_squareexpression {
    private static final double NULL = 0.0000000001;
    public static void main(String[] args) {
        double a, b, c;
        double D, x1, x2, y1, y2;
        System.out.println("This project is calculating result for square expression ax^2+bx+c=0");
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("Please, Enter a number 'a' :  ");
            a = in.nextDouble();
            System.out.println("Please, Enter a number 'b' :  ");
            b = in.nextDouble();
            System.out.println("Please, Enter a number 'c' :  ");
            c = in.nextDouble();

            if (a == 0.0) {
                System.out.println("the expression ax^2+bx+c=0 has no roots");
                return;
            }
            if (b == 0.0 && c == 0.0) {
                System.out.println("this is an incomplete quadratic equation has one solution x = 0");
                return;
            }
            D = (b * b) - (4 * a * c);
            System.out.println("Discriminant = " + D);   //message output to console
            double D1 = Math.sqrt(D);
            if (D <= -NULL) {
                System.out.println("The equation hasn't roots");
            } else if (D <= NULL) {
                x1 = -b / (2 * a);
                System.out.println("The equation has one root x = " + x1);
                y1 = ((a * x1 * x1) + (b * x1) + c);
                System.out.println("checkout ax^2+bx+c= " + y1);
                if ( y1 >= -NULL && y1 <= NULL ){
                    y1=0;
                }

            } else {
                x1 = (-b - D1) / (2 * a);
                x2 = (-b + D1) / (2 * a);
                System.out.println("The equation has two roots: x1 = " + x1 + " and x2 = " + x2);
                y1 = ((a * x1 * x1) + (b * x1) + c);
                y2 = ((a * x2 * x2) + (b * x2) + c);
                if ( y1 >= -NULL && y1 <= NULL ){
                    y1=0;
                }
                if ( y2 >= -NULL && y2 <= NULL ){
                    y2=0;
                }

                System.out.println("checkout x1 ax^2+bx+c= " + y1);
                System.out.println("checkout x2 ax^2+bx+c= " + y2);
            }

        } catch (InputMismatchException ex) {
            System.out.println("Please, be attentive: you entered not a number.");
        }
    }
}
